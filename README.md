# Docker Planka

### Copy `env.sample` to `.env` 
```bash
cp env.sample .env
```

### Configure `.env`

### Start docker
```bash
docker compose up -d
```

### Fix `data/` directory ownership
```bash
sudo chown 1000:1000 -R data
```
